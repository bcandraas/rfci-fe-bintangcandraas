import java.util.*

fun main() {
    val input = Scanner(System.`in`)
    print("Masukkan input = ")
    val nums = input.nextLine().split(" ")
    val arrNums = IntArray(nums.size)
    for (i in 0 until arrNums.size){
        arrNums[i] = (nums[i].toInt())
    }
    var temp: Int
    var n = 0
    var res = ""
    var j = 0
    for (i in 0..1000) {
        if (j > arrNums.size-2) {
            break
        } else{
            if (arrNums[j] > arrNums[j + 1]) {
                temp = arrNums[j + 1]
                arrNums[j + 1] = arrNums[j]
                arrNums[j] = temp
                n++
                arrNums.forEach {
                    res += "$it "
                }
                println("$n. [${arrNums[j]},${arrNums[j + 1]}] -> $res")
                res = ""
                j = -1
            }
            j++
        }
    }
    println("Jumlah swap: $n")
}